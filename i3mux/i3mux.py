#!/usr/bin/python3

import subprocess as sp
from argparse import ArgumentParser

from i3ipc import Connection

parser = ArgumentParser(description='Helper for i3-tmux bindings')
parser.add_argument('cmd', type=str)

def get_ssh_pinfo():
    ps = sp.run(['ps', 'ux'], stdout=sp.PIPE)
    lines = ps.stdout.decode('utf-8').split('\n')

    # Accumulated tuples of pts and corresponding ssh-command
    res = {}
    for line in lines:
        if 'ssh' in line:
            l = line.split()
            pts = l[6]
            c = l[10:]
            if not '-W' in c:
                res[pts] = c
    return res



def main():
    args = parser.parse_args()

    i3 = Connection()
    win_focused = i3.get_tree().find_focused()

    title = win_focused.window_title

# Common tmux command prefix string
    tmux_prefix = 'tmux -CC'

# Prefix for what command to send
    ssh_prefix = []

    if 'ssh' in title and 'pts' in title:
        #XXX this depends on the window title set via xdotool (or better, yet
        #    currently unknown, ways)
        pts = title.split(':')[1]
        # Find corresponding ssh command
        ssh_prefix = get_ssh_pinfo().get(pts, [])

        # ssh needs a pseudo-terminal to properly send tmux commands
        ssh_prefix.insert(1, '-t')

    if ssh_prefix:
        tmux_command = ' '.join([tmux_prefix, args.cmd])
        sp.run(ssh_prefix + [tmux_command])
    else:
        #XXX Command arguments with spaces do not work in list mode somehow
        command = '{} {}'.format(tmux_prefix, args.cmd)
        sp.run(command, shell=True)


if __name__ == "__main__": main()
