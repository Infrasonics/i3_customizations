#!/bin/bash

TITLE_PREV="$(xdotool getwindowname $(xdotool getactivewindow))"

TTY=$(tty)

# Set to format "ssh:pts/0"
xdotool set_window --name "ssh:${TTY#/dev/}" $(xdotool getactivewindow)

# Restore previous title
ssh ${*} && \
xdotool set_window --name "${TITLE_PREV}" $(xdotool getactivewindow)

