#!/usr/bin/python3

import json
import subprocess

import objectpath as opath

def get_tree_full():
    proc = subprocess.run(['i3-msg', '-t', 'get_tree'], stdout=subprocess.PIPE)
    return json.loads(proc.stdout)


def get_workspace_info():
    proc = subprocess.run(['i3-msg', '-t', 'get_workspaces'], stdout=subprocess.PIPE)
    return json.loads(proc.stdout)


#TODO does not find subproperties like title, class, etc.
def find_kv_pair(tree, k_wanted, v_wanted):
    res = []

    for k, v in tree.items():

        if k == 'nodes' or k == 'floating_nodes':
            if isinstance(v, dict):
                res.extend(find_kv_pair(v, k_wanted, v_wanted))
            elif isinstance(v, list):
                for l in v:
                    res.extend(find_kv_pair(l, k_wanted, v_wanted))
        else:
            if isinstance(v_wanted, bool):
                if v is v_wanted:
                    res.append(tree)
            else:
                if v == v_wanted:
                    res.append(tree)
    return res


def get_focused_container():
    tree = get_tree_full()
    opt = opath.Tree(tree)
    res = opt.execute('$..*[@.type is con and @.focused is True]')
    res = list(res)
    if len(res) == 1: return res[0]
    else: raise ValueError


def get_focused_workspace():
    list_ws = get_workspace_info()
    for ws in list_ws:
        if ws["focused"] is True:
            return ws


def get_all_of_class(tree, name_class):
    tree = get_tree_full()
    opt = opath.Tree(tree)
    res = opt.execute('$..*[@.type is con and @.window_properties.class is %s]'
                      % name_class)
    return list(res)

def get_current_workspace_floating_ids():
    ws = get_focused_workspace()
    tree = get_tree_full()
    opt = opath.Tree(tree)
    res = opt.execute('$..*[@.type is workspace and @.num is %d].floating_nodes' +
                      '..*[@.type is floating_con].*[@.id]') % (ws['num'],)
    return list(res)

