#!/usr/bin/python3

import os

from i3ipc import Connection

i3 = Connection()
dirpath = os.path.dirname(i3.socket_path)

marks = i3.get_marks()
workspace_focused = [ws for ws in i3.get_workspaces() if ws.focused][0]

# Construct a tag for the focused workspace
mark = 'hidden_' + str(workspace_focused.num)

#TODO Think about adding floating nodes when called multiple times

# Check whether there are any hidden containers for this ws
if any([m.startswith(mark) for m in marks]):
    # Get all ids of hidden containers
    con_marked = i3.get_tree().find_marked(mark + '*')
    for con in con_marked:
        # Find correct hidden indicator in case of multiple marks
        indicator = [m for m in con.marks if m.startswith(mark)]

        if len(indicator) == 1:
            i3.command('[con_mark="{}"] scratchpad show'.format(*indicator,))
            i3.command('[con_mark="{}"] unmark {}'.format(*indicator, *indicator))
        else:
            #TODO handle error
            pass
else:
    # Find all floating containers on focused workspace
    #XXX This works bottom up so far, there might be a better way to do this
    con_to_mark = [con for con in [c for c in i3.get_tree().leaves() if c.floating == 'user_on']
                   if con.workspace().name == workspace_focused.name]
    # As marks are unique, number them
    for i, con in enumerate(con_to_mark):
        index = mark + '-' + str(i)
        i3.command('[con_id="{:d}"] mark {}, [con_id="{:d}"] move scratchpad'
                   .format(con.id, index, con.id))

