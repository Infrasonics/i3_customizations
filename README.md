# i3_customizations


## i3mux / tmux-mode

Proof of concept wrapper for using a `tmux-mode` of [i3wm](https://i3wm.org/) to
control [tmux](https://tmux.github.io/) locally and in ssh-sessions.


### Installation

- Install (e.g. via `pip`) the python package [i3ipc](https://github.com/altdesktop/i3ipc-python)
- Put the `i3mux_ssh_wrapper.sh` and the `i3mux.py` scripts into your PATH.
- Configure your .i3/config to your liking. (See example snippet below.)

In order to check whether the current window has a running ssh session
the following workaround is suggested:
    Set the window title to "ssh:pts/X" when running ssh. For this purpose
    a wrapper script is included and might be used in the fashion of
    `alias ssh='i3mux_ssh_wrapper.sh'`

To make the lagging bearable, something along these lines in your .ssh/config
might help:
    ```
    Host *
        ControlMaster auto
        ControlPath ~/.ssh/master-sockets/%r@%h:%p
    ```

### Requirements:
    - `i3ipc.py`
    - `tmux`
    - `xdotool`

### Features:
    - Use your window manager keybindings for tmux.
    - When used with the wrapper, detects `ssh`-sessions and sends the commands there.

### Caveats:
    - When using the resize command on a remote session it generates a lot of
      traffic, it might be a bad idea to keep the keys pressed like doing
      locally.

### Bugs:
    - Not snappy enough, should be coded in a compiled language.
    - Doesn't handle nested sessions.
    - Only tested for one tmux session running, might need expansion to support
      multiple ones and some kind of prompt for the session name.


### i3 config sample
```
set $tmxcmd exec --no-startup-id ${HOME}/usr/bin/i3mux.py
mode "tmux" {

    bindsym $mod+j          $tmxcmd "select-pane -L"
    bindsym $mod+k          $tmxcmd "select-pane -D"
    bindsym $mod+l          $tmxcmd "select-pane -U"
    bindsym $mod+odiaeresis $tmxcmd "select-pane -R"

    bindsym $mod+u    $tmxcmd "previous-window"
    bindsym $mod+p    $tmxcmd "next-window"

    bindsym $mod+v    $tmxcmd "split-pane -h"
    bindsym $mod+h    $tmxcmd "split-pane -v"

    bindsym $mod+Shift+c $tmxcmd "kill-pane"
    bindsym $mod+Shift+q $tmxcmd "kill-window"

    #fullscreen/zoomed
    bindsym $mod+Shift+f $tmxcmd "resize-pane -Z"

    bindsym $mod+Return  $tmxcmd "new-window"

    bindsym $mod+0 $tmxcmd "select-window -t 0"
    bindsym $mod+1 $tmxcmd "select-window -t 1"
    # ...

    # back to normal
    bindsym $mod+Escape mode "default"
    bindsym $mod+r mode "tmux-resize"
}

mode "tmux-resize" {
    bindsym j          $tmxcmd "resize-pane -L 5"
    bindsym k          $tmxcmd "resize-pane -D 5"
    bindsym l          $tmxcmd "resize-pane -U 5"
    bindsym odiaeresis $tmxcmd "resize-pane -R 5"

    # Back to tmux normal mode
    bindsym Escape mode "tmux"
    bindsym Return mode "tmux"
}

bindsym $mod+Shift+t mode "tmux"
```


## hide_floats

Uses the python `objectpath` module available via pip.

As the title suggests, this script hides all floating containers on the current
workspace and toggles them back when called again. Maybe handy but was started
to try out the ipc so rather hacky.

It works by setting a mark on all floating containers then moving those to the
scratchpad. Using it a second time does simply recall the marked containers.

